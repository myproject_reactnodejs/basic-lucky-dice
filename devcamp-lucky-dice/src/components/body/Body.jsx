import { Component } from "react";
import image_default from '../../../src/assets/images/dice.png';
import image_people_playing from '../../../src/assets/images/people-playing-casino.jpg';

import image_Dice1 from '../../../src/assets/images/1.png';
import image_Dice2 from '../../../src/assets/images/2.png';
import image_Dice3 from '../../../src/assets/images/3.png';
import image_Dice4 from '../../../src/assets/images/4.png';
import image_Dice5 from '../../../src/assets/images/5.png';
import image_Dice6 from '../../../src/assets/images/6.png';

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageDice: image_default,
        };
    }

    onBtnNemClick = () => {
        console.log('Nut Nem duoc bam!');
        const vRandomNumber = Math.floor(Math.random() * 6) + 1;
        console.log('Gia tri xuc sac: ' + vRandomNumber);

        const diceImages = [
            null,  // Index 0 is not used
            image_Dice1,
            image_Dice2,
            image_Dice3,
            image_Dice4,
            image_Dice5,
            image_Dice6,
        ];

        this.setState({
            imageDice: diceImages[vRandomNumber],
        });
    };

    render() {
        return (
            <div className="container text-center">
                <img src={image_people_playing} alt="people-playing" style={{ marginTop: '20px' }} />
                <h1>Chào mừng đến với My Lucky Dice</h1>
                <br />
                <img style={{ width: '150px' }} src={this.state.imageDice} alt="dice" />
                <br />
                <button
                    className="btn btn-success"
                    style={{
                        width: '150px',
                        height: '50px',
                        fontSize: '25px',
                        marginTop: '15px',
                    }}
                    onClick={this.onBtnNemClick}
                >
                    Ném
                </button>
            </div>
        );
    }
}

export default Body;

