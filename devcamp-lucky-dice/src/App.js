import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Body from './components/body/Body';

function App() {
  return (
    <div>
      <Body />
    </div>
  );
}

export default App;
